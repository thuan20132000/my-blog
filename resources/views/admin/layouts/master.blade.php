

@include('admin.layouts.header')

@include('admin.layouts.nav')

@include('admin.layouts.leftside')

<div class="content-wrapper">
    @yield('content')
</div>

@include('admin.layouts.footer')

