@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>


        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Quick Example</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{route('topic.update',$topic->id)}}" method="POST" enctype="multipart/form-data" >
                  @csrf
                  @method('PUT')
                    <div class="card-body">
                    <div class="form-group">
                      <label for="topicName">Name</label>
                      <input value="{{$topic->name}}" name="name" type="text" class="form-control" id="topicName" placeholder="Enter Topic Name">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <div class="input-group"  id="uploadForm">
                        <div class="custom-file">
                          <input name="image" type="file" class="custom-file-input" id="file">
                          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append" >
                          <span class="input-group-text" id="">Upload</span>
                        </div>
                      </div>
                      <img src="{{ filter_var($topic->image, FILTER_VALIDATE_URL)?$topic->image:asset('uploads/'.$topic->image) }}" alt="" width="100" height="100">

                    </div>

                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
        </div>
    </div>
@endsection
