
@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Responsive Hover Table</h3>

          <div class="card-tools">
            <a href="{{route('post.create')}}" class="btn btn-success" style="margin-left:100">Add New</a>

            <div class="input-group input-group-sm" style="width: 150px;">

              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-danger" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th>Image</th>
                <th>Topic</th>
                <th>Settings</th>
              </tr>
            </thead>
            <tbody>

                @foreach ($posts as $key => $post)
                    <tr>
                        <td>{{$key}}</td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->description}}</td>

                        <td>
                            <img src="{{ filter_var($post->image, FILTER_VALIDATE_URL)?$post->image:asset('uploads/'.$post->image) }}" alt="" width="100" height="100">
                        </td>
                        <td>
                            <a href="{{route('topic.index',$post->topic->id)}}">{{$post->topic->name}}</a>
                        </td>
                        <td>
                            <a href="{{route('post.edit',$post->id)}}">
                                <i class="far fa-edit nav-icon btn btn-primary"></i>
                            </a>
                            <a href="#" onclick="document.getElementById('formDelete').submit()">
                                <i class="far fa-trash-alt nav-icon btn btn-danger"></i>
                                <form id="formDelete" method="POST" action="{{route('post.destroy',$post->id)}}">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </a>


                        </td>
                    </tr>
                @endforeach


            </tbody>
          </table>
        </div>
        <div style="display:flex;justify-content:center">{{$posts->links()}}</div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
@endsection
