@extends('admin.layouts.master')

@section('content')

    <div class="row">
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>


        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Quick Example</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{route('post.store')}}" method="POST" enctype="multipart/form-data" >
                  @csrf
                    <div class="card-body">
                    <div class="form-group">
                      <label for="topicName">Title</label>
                      <input name="name" type="text" class="form-control" id="topicName" placeholder="Enter Topic Name">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class="form-control" rows="3" placeholder="Enter Description ..."></textarea>
                    </div>
                    <div class="form-group">
                        <label>Select Topic</label>
                        <select class="form-control" name="topic">

                            @foreach ($topics as $topic)
                                <option value="{{$topic->id}}">{{$topic->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input name="image" type="file" class="custom-file-input" id="file">
                          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                          <span class="input-group-text" id="">Upload</span>
                        </div>
                      </div>
                    </div>
                    <div id="uploadForm">
                        {{-- file preview here --}}
                    </div>

                    <div class="form-group">
                        <h3>Content</h3>
                        <textarea name="content" class="textarea" id="" cols="30" rows="10"></textarea>
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
        </div>

    </div>
@endsection
