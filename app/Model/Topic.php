<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Post;

class Topic extends Model
{
    //

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
