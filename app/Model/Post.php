<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Topic;


class Post extends Model
{
    //

    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }
}
