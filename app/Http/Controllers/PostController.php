<?php

namespace App\Http\Controllers;

use App\Http\Resources\Post\PostCollection;
use App\Model\Post;
use App\Model\Topic;
use Illuminate\Http\Request;

class PostController extends Controller
{





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        // $post_total =  Post::orderBy('id','desc')->first()->id;

        $topic_id = $request->topic_id;
        $post_number = $request->post_number?$request->post_number:6;
        $page = $request->page?$request->page:0;


        // meta
        $total = $topic_id?count(Post::where('topic_id',$topic_id)->get()):count(Post::all());
        $current_page =$page>0?$page:1;
        $from  = $post_number * $page;
        $last_page = round($total/$post_number +1);
        $to =$post_number * $page + $post_number;

        //links
        $first = route('post.index',['page'=>$from,'topic_id'=>$topic_id,'post_number'=>$post_number]);
        $last = route('post.index',['page'=>$to,'topic_id'=>$topic_id,'post_number'=>$post_number]);
        $prev = route('post.index',['page'=>$current_page-1,'topic_id'=>$topic_id,'post_number'=>$post_number]);
        $next = route('post.index',['page'=>$current_page+1,'topic_id'=>$topic_id,'post_number'=>$post_number]);

        $pagination_arr = [
            'links'=>[
                'first'=>$first,
                'last'=>$last,
                'prev'=>$prev,
                'next'=>$next
            ],
            'meta'=>[
                'current'=>$current_page,
                'from'=>$from,
                'last_page'=>$last_page,
                'to'=>$to,
                'total'=>$total
            ]
        ];

        if($topic_id){

            return [
                'data'=>Post::where('topic_id',$topic_id)->take($post_number)->get(),
            ];

        }

        return [
            'data'=>Post::where('id','>',$total-($post_number*$current_page))->take($post_number)->get(),
            'pagination'=>$pagination_arr
        ];


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
        return $post;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
