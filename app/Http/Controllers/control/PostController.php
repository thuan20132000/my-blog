<?php

namespace App\Http\Controllers\control;

use App\Http\Controllers\Controller;
use App\Model\Post;
use App\Model\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use PhpParser\Node\Stmt\Throw_;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::orderBy('id','desc')->paginate(20);
        return view('admin.post.index',['posts'=>$posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $topics = Topic::all();
        return view('admin.post.create',['topics'=>$topics]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required',
            'topic'=>'required',
            'image'=>'mimes:png,jpg',
            'content'=>'required',
            'description'=>'required'
        ],[
            'name.require'=>"please enter name"
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'post-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
            }

        }





        $post = new Post();
        $post->topic_id = $request->topic;
        $post->title = $request->name;
        $post->slug = Str::slug($request->name,'-');
        $post->image = $fileName;
        $post->description = $request->description;
        $post->content = $request->content;
        $post->save();

        return redirect()->back()->with('success','Created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id)->first();
        $topics = Topic::all();
        return view('admin.post.edit',['post'=>$post,'topics'=>$topics]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name'=>'required',
            'topic'=>'required',
            'image'=>'mimes:png,jpg',
            'content'=>'required',
            'description'=>'required'
        ],[
            'name.require'=>"please enter name"
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'post-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
            }

        }





        $post = Post::find($id)->first();
        $post->topic_id = $request->topic;
        $post->title = $request->name;
        $post->slug = Str::slug($request->name,'-');
        if($request->image){
            $post->image = $fileName;
        }
        $post->description = $request->description;
        $post->content = $request->content;
        $post->update();

        return redirect()->back()->with('success','Updated successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         //
         try {
            //code...
            Post::find($id)->delete();
        } catch (Throw_ $er) {
            return redirect()->back()->with('failed','Delete Failed');
        }

       return redirect()->back()->with('success','Delete Successfull');
    }
}
