<?php

namespace App\Http\Controllers\control;

use App\Http\Controllers\Controller;
use App\Model\Topic;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $topics = Topic::orderBy('id','desc')->paginate(10);
        return view('admin.topic.index',['topics'=>$topics]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.topic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required',
            'image'=>'mimes:png,jpg'
        ],[
            'name.require'=>"please enter name"
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'topic-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
            }

        }





        $topic = new Topic();
        $topic->name = $request->name;
        $topic->slug = Str::slug($request->name,'-');
        $topic->image = $fileName;
        $topic->save();

        return redirect()->back()->with('success','Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $topic = Topic::find($id)->first();
        return view('admin.topic.edit',['topic'=>$topic]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name'=>'required',
            'image'=>'mimes:png,jpg'
        ],[
            'name.require'=>"please enter name"
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'topic-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
            }

        }





        $topic = Topic::find($id)->first();
        $topic->name = $request->name;
        $topic->slug = Str::slug($request->name,'-');
        if($request->image){
            $topic->image = $fileName;
        }
        $topic->update();

        return redirect()->back()->with('success','Updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            //code...
            Topic::find($id)->delete();
        } catch (Exception $er) {
            return redirect()->back()->with('failed','Delete Failed');
        }

       return redirect()->back()->with('success','Delete Successfull');
    }
}
