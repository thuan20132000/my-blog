    /**
     * created by Thuan
     * created at 27/07/2020
     * description: function to preview image in create and edit
     */
    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#uploadForm + img').remove();
                $('#uploadForm').after('<img src="' + e.target.result + '" width="220" height="220"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#file").change(function() {
        filePreview(this);
    });


