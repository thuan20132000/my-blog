<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Post;
use App\Model\Topic;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Post::class, function (Faker $faker) {
    return [
        //
        'title'=>$faker->name,
        'slug'=>Str::slug($faker->name,'-'),
        'description'=>$faker->paragraph,
        'image'=>$faker->imageUrl,
        'content'=>$faker->text ,
        'topic_id'=>function(){
            return Topic::all()->random();
        }
    ];
});
