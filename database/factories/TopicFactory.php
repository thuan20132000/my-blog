<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Topic;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Topic::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->name,
        'slug'=>Str::slug($faker->name,'-'),
        'image'=>$faker->imageUrl,

    ];
});
